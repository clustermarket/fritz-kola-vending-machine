The badly designed Fritz-Kola Vending Machine
=============================================

![Fritz-Kola](app/assets/images/Fritz-Kola.jpg "Fritz-Kola")


Several silly developers decided to design a vending machine with a web interface written in Ruby on Rails. The machine sells __Fritz-Kolas__ for `£3.37`. The machine is also stocked up in coins with the following denominations: `1p, 5p, 10p, 20p, 50p and £2`.

The machine accepts a single banknote and returns the appropriate change, but because image recognition wasn't included in the software budget users are supposed to just input how much money they will put in.

The machine's web interface should have a single form where you can enter the amount of the banknote you have and how many _Fritz-Kolas you_ want. The system should then return a JSON array containing the minimum number of coins needed to return the correct change. This JSON will feed into some other subsystem that controls the coin dispensing mechanism, but that's for someone else to figure out.

Your task is to write a simple web application that satisfies these requirements along with RSpec tests for the back end. UI tests are not important for this particular vending machine and the UI is not supposed to be very appealing. __Fritz-Kola__ rocks and the people who want it cannot be put off by a bad UI design.

_**Note:** You can use the Rails app in this project. It's based on Rails `6.0.2` running on Ruby `2.7.1`. If you don't have these pre-installed on your computer you can create a new Rails app yourself as installing these specific versions can take a lot of time._

P.S. The logo is in the `app/assets/images/` directory.

Good Luck!
